import numpy as np
import os
import time


def neighbourhood(i,j,board):
    shape= board.shape
    if j==shape[1]-1:
        idx_j = [j-1,j,0]
    elif j==0:
        idx_j = [shape[1]-1,0,1]
    else:
        idx_j = [j-1,j,j+1]

    if i==shape[0]-1:
        idx_i = [i-1,i,0]
    elif i==0:
        idx_i = [shape[0]-1,0,1]
    else:
        idx_i = [i-1,i,i+1]
    # alternatively using the % operator and list comprehension
    # idx_i= [(i+k-1)%board_shape[0] for k in range(3)]
    # idx_j= [(j+k-1)%board_shape[1] for k in range(3)]
    count = 0
    # loop over neighbours
    #for ii in range(3):
    #    for jj in range(3):
    #        if ii!=jj:
    #            count = count + board[idx_i[ii],idx_j[jj]]
    for ii in idx_i:
        for jj in idx_j:
            if not (ii== i and jj==j):
                count = count + board[ii,jj]
 
    # apply rules
    if count ==3:
        return 1
    elif count ==2 and board[i,j]==1:
        return 1
    else:
        return 0



board_shape = (10,10)


# initialize randomly
rng = np.random.default_rng()
board = rng.integers(0, 2, size=(board_shape[0],board_shape[1]),dtype=int) 
# be aware that we enforce pure integer values here, 
# we did not in the previous version...see the potential bug later

#board=np.zeros(board_shape)
#board[4,3]=1
#board[4,4]=1
#board[4,5]=1

print(board)
time.sleep(1)
new_board = np.zeros_like(board) #Your comment was right after all...

for iterations in range(1000):
    for i in range(board_shape[0]):
        for j in range(board_shape[1]):
            new_board[i,j] = neighbourhood(i,j,board)
    board[...] = new_board
    os.system("clear")
    print(new_board)
    time.sleep(0.5)

   
        
        


