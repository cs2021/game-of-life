# for board
import numpy as np
import numpy.typing as npt
# for plotting (does not work currently)
import matplotlib.pyplot as plt
import matplotlib.animation as anim 

# for animation
import os
import time
# for type hints
import typing
from typing import Protocol, Tuple


class Game(Protocol):
    def __init__(self,initial_board: npt.NDArray[np.int32]):
        ...

    def update(self):
        ...

    def board(self) -> npt.NDArray[np.int32]:
        ...

class CliSimulation:
    """ Visualizes a board based game on the command line. """
    def __init__(self, game: Game):
        self.game = game 
        _rows = 10
        _cols = 10
    
    def __call__(self, iterations: int):
        """ 
        Computes frames for animation.

        Parameters:
            iterations (int): number of frames to compute

        """
        for it in range(iterations):
            self._clear()
            self._render(self.game.board())
            self._wait()
            self.game.update()

    def _clear(self):
        os.system("clear")
    
    def _wait(self):
        time.sleep(0.1)

    def _render(self, board: npt.NDArray[np.int32]):
        print(
            np.array2string(board,formatter={"int":lambda x:"#" if x>0 else " "})
        )

class Simulation:
    """ Visualizes a board bases game. """
    def __init__(self, game: Game):
        self.game = game 
        fig, ax = plt.subplots()
        self.fig = fig
        self.ax = ax
        self.board_shape = game.board().shape
        self.p  = self.ax.imshow(np.zeros_like(game.board()), \
                cmap = plt.cm.gray_r, interpolation='None', \
                extent=[0, self.board_shape[1], 0, self.board_shape[0]])

    def __call__(self, iterations: int):
        """ 
        Computes frames for animation.

        Parameters:
            iterations (int): number of frames to compute

        """
        a = anim.FuncAnimation(self.fig, 
                               self._render, 
                               init_func = self._init,
                               frames = iterations, 
                               interval = 200)
        # Add a title
        plt.title('Game Of Life')
        # Show grid lines
        plt.grid(which='both', linestyle='--')
        plt.xticks(ticks=[i for i in range(self.board_shape[0])], \
                labels=[' ' for i in range(self.board_shape[0])])
        plt.yticks(ticks=[i for i in range(self.board_shape[1])], \
                labels=[' ' for i in range(self.board_shape[1])])

        plt.show()

        return a

    def _init(self):
        self.p  = self.ax.imshow(self.game.board(), \
                cmap = plt.cm.gray_r, interpolation='None', \
                extent=[0, self.board_shape[1], 0, self.board_shape[0]])
        return (self.p,)

    def _render(self, i):
        self.game.update()
        self.p.set_data(self.game.board())
        return (self.p,)




