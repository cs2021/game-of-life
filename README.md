# Game Of Life

## Ruleset
The domain is periodic i.e. the right neighbour of the rightmost cells is the leftmost cell. For the other boundaries this applies analogously.

1. Any live cell with fewer than two live neighbours dies, as if by underpopulation.
2. Any live cell with two or three live neighbours lives on to the next generation.
3. Any live cell with more than three live neighbours dies, as if by overpopulation.
4. Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.