
from gol_library import GameOfLife
import numpy as np

def still_lifes():
    boards = []
    tmp = np.zeros((10,10),dtype=int)
    tmp[4:6,4:6] = 1
    boards.append(tmp)
    return boards 

def oscillators():
    boards = []
    in_board = np.zeros((10,10),dtype=int)
    in_board[4:7,5] = 1
    out_board = np.zeros_like(in_board)
    out_board[5,4:7] = 1
    boards.append((in_board,out_board))
    return boards 



def test_gol_still_lifes(): 
    for l in still_lifes():
        gol = GameOfLife(l)
        gol.update()
        if not np.all(l == gol.board()):
            assert False,"Still life was not still."

def test_gol_oscillators():
    for l_i,l_o in oscillators():
        gol=GameOfLife(l_i)
        gol.update()
        if not np.all(l_o == gol.board()):
            assert False,"Oscillators did not work."

def test_gol_benchmark(benchmark):
    board = np.ones((50,50))
    def gol_next(board):
        gol=GameOfLife(board)
        gol.update()
        return gol.board()
    benchmark(gol_next, board)


if __name__ == "__main__":
    test_gol_still_lifes()
    test_gol_oscillators()
