from gol_library import GameOfLife
from game_library import CliSimulation, Simulation
import numpy as np
if __name__=="__main__":
    rng = np.random.default_rng()
    board = rng.integers(0, 2, size=(20,20),dtype=int)

    gol = GameOfLife(board)
    #sim = CliSimulation(gol)
    sim = Simulation(gol)
    sim(500)
