import numpy as np
import numpy.typing as npt
import typing
from typing import Protocol, Tuple




def neighbourhood(i,j,board):
    shape= board.shape
    if j==shape[1]-1:
        idx_j = [j-1,j,0]
    elif j==0:
        idx_j = [shape[1]-1,0,1]
    else:
        idx_j = [j-1,j,j+1]

    if i==shape[0]-1:
        idx_i = [i-1,i,0]
    elif i==0:
        idx_i = [shape[0]-1,0,1]
    else:
        idx_i = [i-1,i,i+1]
    # alternatively using the % operator and list comprehension
    # idx_i= [(i+k-1)%board_shape[0] for k in range(3)]
    # idx_j= [(j+k-1)%board_shape[1] for k in range(3)]
    count = 0
    # loop over neighbours
    #for ii in range(3):
    #    for jj in range(3):
    #        if ii!=jj:
    #            count = count + board[idx_i[ii],idx_j[jj]]
    for ii in idx_i:
        for jj in idx_j:
            if not (ii== i and jj==j):
                count = count + board[ii,jj]
 
    # apply rules
    if count ==3:
        return 1
    elif count ==2 and board[i,j]==1:
        return 1
    else:
        return 0



    
class GameOfLife:
    def __init__(self, 
                 initial_board:  npt.NDArray[np.int32]):
        self._board = initial_board

    def update(self):
        s = self._board.shape
        new_board = np.zeros_like(self._board) #Your comment was right after all...
        for i in range(s[0]):
            for j in range(s[1]):
                new_board[i,j] = neighbourhood(i,j,self._board)
        self._board = new_board
    
    def board(self)->npt.NDArray[np.int32]:
        return np.copy(self._board)


   